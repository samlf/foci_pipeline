# Foci Counting Pipeline

## Prerequisites

* Python 3.7+ is installed
* Python dependencies have been installed with `pip install -r requirements.txt`

## Usage

`python foci_pipeline.py /my/input/path /my/output/path threshold1,threshold2` 

`threshold1,threshold2` is a comma-separated list of foci counting threshold to use, one value per channel. E.g. `15,5`

The pipeline will pause after image stacking to allow you to run Focinator on the output.  
Press ENTER to continue with data analysis once Focinator is complete.  

## Pipeline Overview

1. Relevant images are inspected manually to determine Focinator thresholds.
2. Stack TIFF files from 3 channels to a single 3-channel image.
3. Move stacked TIFF files to output folder structure
4. Focinator is run manually on the image folders. 
5. Analyze focinator output for foci statistics.
6. Load output data into GraphPad Prism and generate graphs. (TODO)

## Notes

### Image Input Folder Structure

The pipeline expects an `input` path pointing to a directory with the following structure:

```
/my/input/path/
├───timepoint_1
│   ├───1_01
│   ├───1_02
│   ├───2_01
│   ├───2_02
│   ├───2_03
│   ├───3_01
│   ├───3_02
│   ├───3_03
│   ├───3_04
│   ├───4_01
│   ├───4_02
│   ├───...
├───timepoint_2
│   ├───...
├───timepoint_3
│   ├───...
└───...
```
Within the input directory is a folder for each time point (or other cohort).  
Within each time point is a folder containing raw images for each plate and well.   
The names of the cohort folders are arbitrary, but the names of the plate/well folders **must** follow `<plate_number>_<well_number>` format.  
Image files must be named like `<MMYYDD>_CH<X>.tif` where `MMYYDD` is the date and `X` is the channel number.

### Stacked Output Folder Structure

The pipeline will stack images and place them in the provided output directory.  
The images will be placed in a folder named based on the input folder structure.  
There will be one output folder of stacked images for each cohort/plate.  
E.g. Input from `/my/input/path/timepoint_1/2_03/` will save output to `/my/output/path/timepoint_1_2/`.
You can run Focinator on each of these folders directly, saving output to the same location.