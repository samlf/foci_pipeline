import logging


class LogMixin:
    @property
    def log(self):
        name = ".".join([__name__, self.__class__.__name__])
        return logging.getLogger(name)
