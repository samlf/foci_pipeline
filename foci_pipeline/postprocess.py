from itertools import chain
from pathlib import Path

import pandas as pd

from stack_images import log


def compute_image_stats(grp, thresholds, foci_col):
    """Calculates image statistics for a single image. 
    Intended to be used in a pd.groupby.apply()
    
    Arguments:
        grp {pd.DataFrame} --  The dataframe containing the current image's data
        thresholds {int} -- foci count amount to be considered "positive"
        foci_col {str} -- the name of the column holding foci counts
    
    Returns:
        cell_count {int} -- Number of cells in the image
        foci_counts {list(Int)} -- Number of positive foci cells in the image, 
        for each channel
        pct_foci {float} -- Ratio of foci_counts to cell_count
    """
    cell_count = len(grp)
    foci_counts = [
        (grp[foci_col + str(i)].astype(int) > thresholds[i - 1]).sum() for i in (1, 2)
    ]
    pct_foci = [(100 * foci_counts[i - 1] / cell_count) for i in (1, 2)]
    return cell_count, foci_counts, pct_foci


def summarize_outfile(outfile, thresholds):
    """Creates a DataFrame of the per-image statistics found in `outfile`.
    
    Arguments:
        outfile {str} -- path of the Focinator outfile to summarize
        thresholds {List(int)} -- foci count amount to be considered "positive", 
        for each channel
    
    Returns
        pd.Dataframe -- the results dataframe with one row per image/channel
    """
    data = pd.read_csv(outfile, sep="\t", na_values="NA", encoding="latin-1")
    # Remove any "mean" rows
    data = data[~(data["Cell"] == "mean")]
    # Determine name of foci count column
    foci_col = data.columns[[i.startswith("Foci") for i in data.columns]][0]
    if "Count" in foci_col:
        foci_col = "Foci Count "
    elif "Channel" in foci_col:
        foci_col = "Foci Channel "
    else:
        raise ValueError("Unknown Foci Count column name (expected Count or Channel)")
    # Ensure datatype
    int_cols = ["Cell on coverslip"] + [foci_col + str(i) for i in (1, 2)]
    data.loc[:, int_cols] = data.loc[:, int_cols].astype(int)
    # Determine which rows belong to which image
    data.loc[data["Cell on coverslip"] == 1, "Image ID"] = range(
        (data["Cell on coverslip"] == 1).sum()
    )
    data["Image ID"] = data["Image ID"].fillna(method="ffill")
    # Create results
    data_grouped = data.groupby("Image ID")
    results_index = sorted(data["Image ID"].unique().tolist() * 2)
    results = pd.DataFrame(
        {
            "Channel": [1, 2] * data_grouped.ngroups,
            "Cell Count": pd.concat(
                [data_grouped["Cell"].count()] * 2, ignore_index=False
            ).sort_index(),
        },
        index=results_index,
    )
    results.index.name = "Image"
    stats = [compute_image_stats(g, thresholds, foci_col) for _, g in data_grouped]
    results["Foci Positive Count"] = list(chain.from_iterable([i[1] for i in stats]))
    results["Foci Positive Percent"] = list(chain.from_iterable([i[2] for i in stats]))
    results = results.reset_index()
    results["Overall Foci Percent"] = [
        100
        * results.loc[results["Channel"] == i, "Foci Positive Count"].sum()
        / results.loc[results["Channel"] == i, "Cell Count"].sum()
        for i in (1, 2)
    ] * results["Image"].nunique()
    results = results.sort_values(by=["Channel", "Image"])
    return results


def process_directory(input_path, thresholds):
    """Operates to produce results for each `outFile.xls` in a directory, searched 
    recursively, and a final summary file in the root.
    
    Arguments:
        input_path {str} -- Path of the experiment folder, under which images are 
        organized
        thresholds {List(int)} -- foci count amount to be considered "positive", for 
        each channel
    """
    input_path = Path(input_path)
    outfiles = input_path.rglob("outFile.xls")
    results = {}
    # Create results for each individual plate/Focinator run
    for o in outfiles:
        log.info(f"Processing file {o}")
        try:
            result = summarize_outfile(o, thresholds)
            results[o] = result
            out_name = f"outFile_summary_{o.parts[-2]}.xlsx"
            result.to_excel(o.parent / out_name, index=False)
        except Exception as e:
            log.warning(f"Failed to process {o}: {e}")
    # Combine all results to a single output file too
    results_df = pd.concat(results.values(), ignore_index=True)
    results_df["Input"] = list(
        chain.from_iterable([[str(i)] * len(results[i]) for i in results])
    )
    results_df = results_df[["Input"] + results_df.columns[:-1].tolist()]
    results_df.to_excel(
        input_path / f"{input_path.parts[-1]}_all_summaries.xlsx", index=False
    )


if __name__ == "__main__":
    process_directory(
        r"D:\Dropbox\#Jam\pipeline\focinator 010320 test structure", thresholds=[15, 5]
    )
