import logging
import os
from pathlib import Path
import re
from typing import Union, List

import numpy as np
from skimage import io

log = logging.getLogger(__name__)

CHANNEL_PATTERN = re.compile(r".+_CH\d\.tif")


def extract_channel(image: np.ndarray):
    """Finds the non-zero channel in an image and returns it as a 2D array.
    
    Arguments:
        image {np.ndarray} -- a 3-channel TIFF image array (rows, columns, channels).
    
    Returns:
        np.ndarray -- a 2-channel TIFF image array (rows, columns).
    """
    for c in range(image.shape[0]):
        # Find the channel that is non-zero
        if np.all((image[:, :, c] == 0)):
            continue
        else:
            out = image[:, :, c]
            break
    return out


def stack_channels(input: Union[str, Path, List[np.ndarray]]) -> np.ndarray:
    """Stacks the single-channel TIFF images given by `input`.
    `input` can be either:
        A list of channel images (np.ndarray), in which case the data-containing 
            channel is extracted from each channel image and the results are stacked.
        A path,  in which case stack_channels performs the previous procedure on each 
            channel image present at the path.
    It is expected that a given path will contain an image for each channel, named 
    "MMDDYY_CHX.tif", where MMDDYY denotes the date and X denotes the channel number.

    Arguments:
        input (Union[str, Path, np.ndarray]) -- TIFF channel images (or their directory)

    Returns:
        np.ndarray -- stacked TIFF image array
    """
    if isinstance(input, (str, Path)):
        image_paths = [i for i in os.listdir(input) if CHANNEL_PATTERN.match(i)]
        images = [io.imread(input / i) for i in image_paths]
    elif isinstance(input, list) and isinstance(input[0], np.ndarray):
        images = input
    else:
        raise TypeError("Expected file path or image data.")
    channels = [extract_channel(i) for i in images]
    stack = np.stack(channels, axis=0)
    return stack


def stack_directory(input_path, output_path, overwrite=False):
    """Stacks all images in `input_path` and saves them in `output_path`.
    The directory is searched recursively for "MMDDYY_CH1.tif" files, and the output
    files are named with their directory path. 
    
    Arguments:
        input_path {str} -- input directory
        output_path {str} -- output directory
        overwrite {Boolean} -- overwrites existing output files if True
    """
    # Get the Path to each image
    input_path = Path(input_path)
    output_path = Path(output_path)
    image_paths = [
        # Get the path for each .tif file matching the predefined regex
        i.parent for i in input_path.rglob("*.tif") if CHANNEL_PATTERN.search(str(i))
    ]
    # Drop duplicates
    image_paths = set(image_paths)
    for p in image_paths:
        # Separate out the top directory, timepoint directory, and plate from each Path
        topdir_idx = p.parts.index(input_path.parts[-1])
        timepoint = p.parts[topdir_idx + 1]
        plate = p.parts[topdir_idx + 2].split("_")[0]
        # Use them to create the output filename for each timepoint directory
        out_name = "_".join(p.parts[topdir_idx:])
        out_name += "_stack.tif"
        out_dir = f"{timepoint}_{plate}"
        if not os.path.exists(output_path / out_dir):
            os.makedirs(output_path / out_dir)
        if (output_path / out_dir / out_name).exists() and not overwrite:
            log.info(f"Skipping {p}, already exists")
            continue
        log.info(f"Stacking {p}")
        try:
            # Stack the channel images in each timepoint directory and save the output
            stack = stack_channels(p)
            io.imsave(output_path / out_dir / out_name, stack)
        except Exception as e:
            log.warning(f"Failed stacking {p}: {e}")
            continue


if __name__ == "__main__":
    stack_directory("./data/011720", "./data/test_output")
