import numpy as np
from foci_pipeline.stack_images import extract_channel, stack_channels


def test_stack_channels():
    images = [np.zeros((3, 3, 3)), np.zeros((3, 3, 3)), np.zeros((3, 3, 3))]
    images[0][:, :, 0] = 1
    images[1][:, :, 1] = 2
    images[2][:, :, 2] = 3
    stack = stack_channels(images)
    assert np.array_equal(
        stack,
        np.stack(
            [np.ones((3, 3)) * 1, np.ones((3, 3)) * 2, np.ones((3, 3)) * 3], axis=0
        ),
    )


def test_extract_nonzero_channel():
    image = np.stack(
        (np.zeros((3, 3)), np.zeros((3, 3)), np.ones((3, 3)), np.zeros((3, 3))), axis=2
    )
    nonzero_channel = extract_channel(image)
    assert np.all(nonzero_channel)
